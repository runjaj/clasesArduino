const int LED = 6; // the pin of the led
int i = 0; // counter

void setup() {
  // put your setup code here, to run once:
  pinMode(LED, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  for (i = 0; i < 255; i++) {
    analogWrite(LED, i);
    delay(10);
  }
  for (i = 255; i > 0; i--) {
    analogWrite(LED, i);
    delay(10);
  }
  analogWrite(LED, 0);
  delay(1000);


}

