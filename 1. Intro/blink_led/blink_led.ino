const int LEDpin = 6;
; // El LED del shield

void setup() {
// Instrucciones de configuración que solo se ejecutarán una vez:
  pinMode(LEDpin, OUTPUT);
}

void loop() {
  // Este código se ejecutará de manera repetida:
  digitalWrite(LEDpin, HIGH);
  delay(1000);
  digitalWrite(LEDpin, LOW);
  delay(1000);

}
