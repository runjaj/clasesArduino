const int LED = 6; // El led de la placa
const int BUTTON = 0;
int val = LOW;  // Indica si el botón está pulsado (HIGH = 1) o no (LOW = 0)
int old_val = LOW;  // Guarda el estado anterior para evitar problemas de rebote del botón


void setup() {
  // put your setup code here, to run once:

  pinMode(LED, OUTPUT);
  pinMode(BUTTON, INPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  int val = digitalRead(BUTTON);
   if ((val == HIGH) && (old_val == LOW)){
    digitalWrite(LED, HIGH);
    delay(10);
  }
  if ((val == LOW) && (old_val == HIGH)){
    digitalWrite(LED, LOW);
    delay(10);
  }
  old_val = val;

}
