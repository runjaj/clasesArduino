const int lmPin = A0; // El pin de lectura del LM35 es el A0 (Analog 0)
const int TIP31 = 3; // El pin del TIP31
const int LED = 6; // El pin del LED (no el de la placa, que es el 13)
const float sp = 58.0; // Consigna
const float hist = 1.0; // Histeresis
const long REFRESH_INTERVAL = 1000; // ms
long lastRefreshTime = 0;
int calef = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // Abre el monitor serie, velocidad 9600
  pinMode(LED, OUTPUT);
  pinMode(TIP31, INPUT);
  //analogReference(INTERNAL);
}

float tempC() // Función de lectura de la temperatura
{
  float raw = analogRead(lmPin);
  float percent = raw / 1023.0;
  float voltage = percent * 5;
  return voltage*100.0-273.15;
}

void loop() {
  // put your main code here, to run repeatedly:
  unsigned long now = millis();
  float temp = tempC();
  if (temp > (sp + hist)){
    calef = 0;
  }

  if (temp < (sp - hist)){
    calef = 200;
  }

  analogWrite(LED, calef);
  analogWrite(TIP31, calef);

  if (millis() - lastRefreshTime >= REFRESH_INTERVAL){
    lastRefreshTime += REFRESH_INTERVAL;
    Serial.print(now);
    Serial.print(", ");
    Serial.print(calef);
    Serial.print(", ");
    Serial.println(temp);
  }
  delay(500);
  
}
