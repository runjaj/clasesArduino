import grafica.*;

import processing.serial.*;
Serial myPort;
String val; // Read value of temperature
float tempC; // Temperature in C
int yDist; // Distance in the y axis to draw the graph
// float[] tempHistory = new float[100]; // Historia, se representan los 100 últimos puntos
Table table; // Table to be save as a csv file
int calef;
float ms;
float sp;
float cP, cI, cD;
GPointsArray points = new GPointsArray(100);
GPointsArray points1 = new GPointsArray(100);
GPlot plot, plot1;
String filename;


void setup()
{
    printArray(Serial.list());
    String portName = Serial.list()[3]; // Port used by the arduino
    myPort = new Serial (this, portName, 9600); // Starts the port
    size(620,650); // Window with the representation of the data
    // Asigna un valor de temperatura inicial de 0


    //for (int index = 0; index < 100; index++)
    //{
    //  tempHistory[index]=0;
    //}
    
    // Creates the table with the data in two columns
    table = new Table();
    table.addColumn("index");
    table.addColumn("ms");
    table.addColumn("calef");
    table.addColumn("TempC");
    //table.addColumn("Tsp");
    //table.addColumn("cP");
    //table.addColumn("cI");
    //table.addColumn("cD");
    
    plot = new GPlot(this);
   
    plot1 = new GPlot(this);
    
    filename = "data/new_"+hour()+minute()+second()+".csv";
  
} 

void draw()
{
  // printArray(Serial.list());
  
  // While arduino is sending data
  if (myPort.available() > 0)
  {
    // Reads a value
    val = myPort.readStringUntil('\n');
    // Converts the reading in a number (temperature in C)
    if(val != null){
      String[] q = splitTokens(val,", ");
      if (q.length== 3){
        ms = float(q[0]);
        calef = int(q[1]);
        tempC = float(q[2]);
        //sp = float(q[3]);
        //cP = float(q[4]);
        //cI = float(q[5]);
        //cD = float(q[6]);
        // Adds a new row to the table and updates the file
        TableRow newRow = table.addRow();
        newRow.setInt("index",table.getRowCount()-1);
        newRow.setFloat("ms", ms);
        newRow.setString("calef", str(calef));
        newRow.setFloat("TempC",tempC);
        //newRow.setFloat("Tsp", sp);
        //newRow.setFloat("cP", cP);
        //newRow.setFloat("cI", cI);
        //newRow.setFloat("cD", cD);
        saveTable(table,filename);
        points.add(ms/1000, tempC);
        points1.add(ms/1000, calef);
      }
    }
    
    
    
    // Clears the window to lose the old data
    background(123);
    
    // Draws the rectangle of the logging graph
    colorMode(RGB, 160);
    stroke(0);
    rect(49,19,22,162);
    
    // Rectangle fading from red to blue
    for (int colorIndex = 0; colorIndex <= 160; colorIndex++) 
    {
      stroke(160 - colorIndex, 0, colorIndex);
      line(50, colorIndex + 20, 70, colorIndex + 20);
    }
    
    
    
    // Draw plot using grafica
    //GPlot plot = new GPlot(this);

    plot.setPoints(points);
    plot.setPos(145,19);
    plot.setTitleText("Controlled Process Variable (PV)");
    plot.getXAxis().setAxisLabelText("t (s)");
    plot.getYAxis().setAxisLabelText("T (ºC)");
    plot.defaultDraw();
    
    // Heating graph
    plot1.setPos(145,330);
    plot1.setTitleText("Manipulable variable");
    plot1.getXAxis().setAxisLabelText("t (s)");
    plot1.getYAxis().setAxisLabelText("Heat.");
    plot1.setPoints(points1);
    plot1.defaultDraw();
    
    ////draw graph
    //stroke(0);
    //fill(255,255,255);
    //rect(90,80,100,100);
    //for (int index = 0; index<100; index++)
    //{
    //  if(index == 99)
    //    tempHistory[index] = tempC;
    //  else
    //    tempHistory[index] = tempHistory[index + 1];
    //    point(90+index, 180-tempHistory[index]); 
    //}
    //write reference values
     fill(0,0,0);
    textAlign(RIGHT);
     text("100 C", 45, 25); 
     text("0 C", 45, 187);
     
     
    //draw triangle pointer
     yDist = int(160 - (160 * (tempC / 100)));
     stroke(0);
     triangle(75, yDist + 20, 85, yDist + 15, 85, yDist + 25);
       //write the temp in C and F
       fill(0,0,0);
      textAlign(LEFT);
       text(str(tempC) + " ºC", 90, yDist+25);
   
     text("Tsp = " + str(sp) + "ºC", 40, 350);
     text("cP = " + str(cP), 40, 370);
     text("cI = " + str(cI), 40, 390);
     text("cD = " + str(cD), 40, 410);

  }
  // Shows the values in the console
  println(val);
}
