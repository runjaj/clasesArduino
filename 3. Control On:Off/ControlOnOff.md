##Control On/Off

En la clase de hoy estudiaréis:

- el funcionamiento de un controlador on/off,
- el efecto de la histéresis del controlador todo/nada en el rendimiento del sistema de control,
- evaluar el rendimiento de un controlador,
- estimar el consumo de energía del sistema de calefacción.

###Preguntas

1. Haz un diagrama del lazo de control mostrando sus principales elementos (proceso, elemento final de control, controlador, etc) y variables (controlada, manipulable, consigna, etc).
2.  ¿Qué es la histéresis de un controlador on/off? Explica el funcionamiento del lazo este tipo de controladores.
2. Vas a comprobar el funcionamiento del lazo de control tomando una temperatura de consigna de 50 ºC y tres valores de histéresis: 2, 1 y 0.5 ºC. El funcionamiento del lazo de control será oscilatorio. ¿Puedes predecir la amplitud y frecuencia para cada uno de estos casos?
3. Respecto al consumo de energía, ¿depende del valor de la histéresis, es decir, será el mismo valor en los tres casos? Si no es así, ¿aumentará o disminuirá el consumo al aumentar la histéresis?
3. Tras realizar los tres experimentos, mide la amplitud y frecuencia de cada uno de los casos. ¿Coinciden con tus predicciones? Explica las diferencias que hayan aparecido.
4. Comprueba tu hipótesis de la pregunta 4., ¿se ajusta a tu predicción?
5. Para acabar, ¿qué valor de histéresis proporciona un mejor control? Busca un parámetro o parámetros que cuantifiquen el rendimiento del controlado.