import grafica.*;

import processing.serial.*;
Serial myPort;
String val; // Valor leído de temperatura
float tempC; // Temperatura en C
int yDist; // Distancia en el eje y para representar la gráfica
// float[] tempHistory = new float[100]; // Historia, se representan los 100 últimos puntos
Table table; // Tabla que se grabará como fichero csv
int calef;
float ms;
float sp;
float cP, cI, cD;
GPointsArray points = new GPointsArray(100);
GPointsArray points1 = new GPointsArray(100);
GPlot plot, plot1;
String filename;


void setup()
{
    printArray(Serial.list());
    String portName = Serial.list()[9]; // Puerto utilizado por el arduino, para averiguarlo printArray(Serial.list())
    myPort = new Serial (this, portName, 9600); // Inicializa el puerto
    size(600,700); // Ventana con la representación de los datos
    // Asigna un valor de temperatura inicial de 0


    //for (int index = 0; index < 100; index++)
    //{
    //  tempHistory[index]=0;
    //}
    
    // Crea la tabla con los datos con dos columnas
    table = new Table();
    table.addColumn("index");
    table.addColumn("ms");
    table.addColumn("calef");
    table.addColumn("TempC");
    table.addColumn("Tsp");
    table.addColumn("cP");
    table.addColumn("cI");
    table.addColumn("cD");
    
    plot = new GPlot(this);
   
    plot1 = new GPlot(this);
    
    filename = "data/new_"+hour()+minute()+second()+".csv";
  
} 

void draw()
{
  // printArray(Serial.list());
  
  // Mientras lleguen datos del arduino
  if (myPort.available() > 0)
  {
    // Lee un valor
    val = myPort.readStringUntil('\n');
    // Convierte la lectura en un número con la temperatura en C
    if(val != null){
      String[] q = splitTokens(val,", ");
      if (q.length== 7){
        ms = float(q[0]);
        calef = int(q[1]);
        tempC = float(q[2]);
        sp = float(q[3]);
        cP = float(q[4]);
        cI = float(q[5]);
        cD = float(q[6]);
        // Añade una nueva fila a la tabla y actualiza el fichero
        TableRow newRow = table.addRow();
        newRow.setInt("index",table.getRowCount()-1);
        newRow.setFloat("ms", ms);
        newRow.setString("calef", str(calef));
        newRow.setFloat("TempC",tempC);
        newRow.setFloat("Tsp", sp);
        newRow.setFloat("cP", cP);
        newRow.setFloat("cI", cI);
        newRow.setFloat("cD", cD);
        saveTable(table,filename);
        points.add(ms/1000, tempC);
        points1.add(ms/1000, calef);
      }
    }
    
    
    
    // Borra la ventana para perder los datos antiguos
    background(123);
    
    // Dibuja el rectánguo de la gráfica de registro
    colorMode(RGB, 160);
    stroke(0);
    rect(49,19,22,162);
    
    // Rectángulo con degradado de rojo a azul
    for (int colorIndex = 0; colorIndex <= 160; colorIndex++) 
    {
      stroke(160 - colorIndex, 0, colorIndex);
      line(50, colorIndex + 20, 70, colorIndex + 20);
    }
    
    
    
    // Draw plot usando grafica
    //GPlot plot = new GPlot(this);

    plot.setPoints(points);
    plot.setPos(145,19);
    plot.setTitleText("Variable controlada");
    plot.getXAxis().setAxisLabelText("t (s)");
    plot.getYAxis().setAxisLabelText("T (ºC)");
    plot.defaultDraw();
    
    // Gráfica calefacción
    plot1.setPos(145,330);
    plot1.setTitleText("Variable manipulable");
    plot1.getXAxis().setAxisLabelText("t (s)");
    plot1.getYAxis().setAxisLabelText("Calef.");
    plot1.setPoints(points1);
    plot1.defaultDraw();
    
    ////draw graph
    //stroke(0);
    //fill(255,255,255);
    //rect(90,80,100,100);
    //for (int index = 0; index<100; index++)
    //{
    //  if(index == 99)
    //    tempHistory[index] = tempC;
    //  else
    //    tempHistory[index] = tempHistory[index + 1];
    //    point(90+index, 180-tempHistory[index]); 
    //}
    //write reference values
     fill(0,0,0);
    textAlign(RIGHT);
     text("100 C", 45, 25); 
     text("0 C", 45, 187);
     
     
    //draw triangle pointer
     yDist = int(160 - (160 * (tempC / 100)));
     stroke(0);
     triangle(75, yDist + 20, 85, yDist + 15, 85, yDist + 25);
       //write the temp in C and F
       fill(0,0,0);
      textAlign(LEFT);
       text(str(tempC) + " ºC", 90, yDist+25);
   
     text("Tsp = " + str(sp) + "ºC", 40, 350);
     text("cP = " + str(cP), 40, 370);
     text("cI = " + str(cI), 40, 390);
     text("cD = " + str(cD), 40, 410);

  }
  // Muestra los valores de temperatura en la consola
  println(val);
}