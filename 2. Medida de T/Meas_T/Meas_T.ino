// Measure temperature using LM335Z

const int TEMPpin = A0; // Pin Analog 0


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // Open serial monitor
}

void loop() {
  // put your main code here, to run repeatedly:
  float raw = analogRead(TEMPpin);
  float voltage = raw/1023.0*5.0; // 10-bit ADC, 5V
  float temp = voltage * 1000 / 10 - 273.15;
  Serial.println(temp);
  delay(1000);

}
