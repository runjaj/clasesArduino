Archivos Arduino/Processing
===========================

1. **Meas_T**: Medida de temperatura básica. Conversión analógica digital. Uso del monitor para ver la temperatura

2. **Medida_Temperatura**: Medida de temperatura definiendo una función

3. **Simple_Kalma**: Uso de un filtro de Kalma. Uso de Processing para ver la temperatura

