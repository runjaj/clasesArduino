const int lmPin = A0; // El pin de lectura del LM35 es el A0 (Analog 0)
const int TIP31 = 3; // El pin del TIP31
const int LED = 13; // El led de la placa

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // Abre el monitor serie, velocidad 9600
  pinMode(LED, OUTPUT);
  //pinMode(TIP31, OUTPUT);
}

float tempC() // Función de lectura de la temperatura
{
  float raw = analogRead(lmPin);
  float percent = raw / 1023.0;
  float milivolts = percent * 5000;
  return milivolts/10.0-273.15;
  //return volts;
}


void loop() {
  // put your main code here, to run repeatedly:

  for(int x = 0; x < 10; x++){
    digitalWrite(LED, LOW);
    analogWrite(TIP31,0);
//    Serial.println();
//    Serial.print(x);
    Serial.print("0, ");
    Serial.println(tempC());
    delay(1000); //ms
  }
  for(int x = 0; x < 600; x++){
    digitalWrite(LED, HIGH);
    analogWrite(TIP31,200);
//    Serial.print("Calor");
//    Serial.println();
//    Serial.print(x);
    Serial.print("1, ");
    Serial.println(tempC());
    delay(1000); //ms
  }
  
}
