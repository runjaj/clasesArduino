import processing.serial.*;
Serial myPort;
String val; // Valor leído de temperatura
float tempC; // Temperatura en C
float tempF; // Temperatura en F
int yDist; // Distancia en el eje y para representar la gráfica
float[] tempHistory = new float[100]; // Historia, se representan los 100 últimos puntos
Table table; // Tabla que se grabará como fichero csv

void setup()
{
    String portName = Serial.list()[5]; // Puerto utilizado por el arduino, para averiguarlo printArray(Serial.list())
    myPort = new Serial (this, portName, 9600); // Inicializa el puerto
    size(360,320); // Ventana con la representación de los datos
    // Asigna un valor de temperatura inicial de 0
    for (int index = 0; index < 100; index++)
    {
      tempHistory[index]=0;
    }
    
    // Crea la tabla con los datos con dos columnas
    table = new Table();
    table.addColumn("index");
    table.addColumn("TempC");
  
} 

void draw()
{
  // printArray(Serial.list());
  
  // Mientras lleguen datos del arduino
  if (myPort.available() > 0)
  {
    // Lee un valor
    val = myPort.readStringUntil('\n');
    // Convierte la lectura en un número con la temperatura en C
    if(val != null) tempC = float(val);
    
    // Añade una nueva fila a la tabla y actualiza el fichero
    TableRow newRow = table.addRow();
    newRow.setInt("index",table.getRowCount()-1);
    newRow.setFloat("TempC",tempC);
    saveTable(table,"data/new.csv");
    
    // Borra la ventana para perder los datos antiguos
    background(123);
    
    // Dibuja el rectánguo de la gráfica de registro
    colorMode(RGB, 160);
    stroke(0);
    rect(49,19,22,162);
    
    // Rectángulo con degradado de rojo a azul
    for (int colorIndex = 0; colorIndex <= 160; colorIndex++) 
    {
      stroke(160 - colorIndex, 0, colorIndex);
      line(50, colorIndex + 20, 70, colorIndex + 20);
    }
    
    //draw graph
    stroke(0);
    fill(255,255,255);
    rect(90,80,100,100);
    for (int index = 0; index<100; index++)
    {
      if(index == 99)
        tempHistory[index] = tempC;
      else
        tempHistory[index] = tempHistory[index + 1];
        point(90+index, 180-tempHistory[index]); 
    }
    //write reference values
     fill(0,0,0);
    textAlign(RIGHT);
     text("50 C", 45, 25); 
     text("0 C", 45, 187);
    //draw triangle pointer
     yDist = int(160 - (160 * (tempC / 50)));
     stroke(0);
     triangle(75, yDist + 20, 85, yDist + 15, 85, yDist + 25);
       //write the temp in C and F
       fill(0,0,0);
      textAlign(LEFT);
       text(str(int(tempC)) + " C", 115, 37);
       tempF = ((tempC*9)/5) + 32;
       text(str(int(tempF)) + " F", 115, 65);   

  }
  // Muestra los valores de temperatura en la consola
  println(val);
}