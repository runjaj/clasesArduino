int lmPin = A0; // El pin de lectura del LM35 es el A0 (Analog 0)


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // Abre el monitor serie, velocidad 9600
}

float tempC() // Función de lectura de la temperatura
{
  float raw = analogRead(lmPin);
  float percent = raw / 1023.0;
  float milivolts = percent * 5000;
  return milivolts/10.0-273.15;
  //return tempC;
}


void loop() {
  // put your main code here, to run repeatedly:
  Serial.println(tempC());
  delay(1000); //ms
}
