# Clases de control de procesos con Arduino

## Organización de las clases

Tema              | Contenido
------------------|------------
1. Intro          | Parpadear LED. Usar un botón. Intro Arduino 
2. Medida T       | Medición y registro de T. Intro Processing
3. Control On/Off | Control On/Off. ¿Cómo medir el rendimiento?
4. Step           | Modelización de un proceso
5. Control PID    | Control P, PI, PD y PID


