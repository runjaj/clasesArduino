const int lmPin = A0; // El pin de lectura del LM35 es el A0 (Analog 0)
const int TIP31 = 3; // El pin del TIP31
// const int LED = 6; // El led de la placa
int initHeight = 60;
int stepHeight = 50;
const long REFRESH_INTERVAL = 2000; // ms
long lastRefreshTime = 0;

// kalman variables
float varVolt = 0.084317983645123;  // variance determined using excel and reading samples of raw sensor data
float varProcess = 1e-4;
float Pc = 0.0;
float G = 0.0;
float P = 1.0;
float Xp = 0.0;
float Zp = 0.0;
float Xe = 0.0;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600); // Abre el monitor serie, velocidad 9600
  // pinMode(LED, OUTPUT);
  // pinMode(TIP31, OUTPUT);
  //analogReference(EXTERNAL);
}

float tempC() // Función de lectura de la temperatura
{
  float raw = analogRead(lmPin);
  float percent = raw / 1023.0;
  float voltage = percent * 5;

  // kalman process
  Pc = P + varProcess;
  G = Pc/(Pc + varVolt);    // kalman gain
  P = (1-G)*Pc;
  Xp = Xe;
  Zp = Xp;
  Xe = G*(voltage-Zp)+Xp;   // the kalman estimate of the sensor voltage
  
  return Xe*100.0-273.15;

}

void loop() {
  // put your main code here, to run repeatedly:

  while(millis()/1000<300){
    unsigned long now = millis();
    // analogWrite(LED, initHeight);
    analogWrite(TIP31,initHeight);
    if (millis() - lastRefreshTime >= REFRESH_INTERVAL){
      lastRefreshTime += REFRESH_INTERVAL;
      Serial.print(now);
      Serial.print(", ");
      Serial.print(initHeight);
      Serial.print(", ");
      Serial.println(tempC());
    }
    delay(200);
  }
  
  unsigned long now = millis();
  // analogWrite(LED, initHeight + stepHeight);
  analogWrite(TIP31,initHeight + stepHeight);
  if (now - lastRefreshTime >= REFRESH_INTERVAL){
    lastRefreshTime += REFRESH_INTERVAL;
    Serial.print(now);
    Serial.print(", ");
    Serial.print(initHeight+stepHeight);
    Serial.print(", ");
    Serial.println(tempC());
  }
  delay(200);
   
}
