# Introducción a los sistemas lineales

1. **Identifica el orden de los siguientes procesos lineales:**

	a) $3 \frac{\mathrm{d} V}{\mathrm{d} t} + V = 4 t$

	b) $a \frac{\mathrm{d}^2 Q}{\mathrm{d} t^2}+b \frac{\mathrm{d} Q}{\mathrm{d} t} + Q = c \sin(t)$

	c) $5 \frac{\mathrm{d} c(t)}{\mathrm{d} t} + \frac{c(t)}{V} = g(t)$

2. **Determina la ganancia y la constante de tiempo para los siguientes sistemas de primer orden:**

	a) $7 \frac{\mathrm{d} f(t)}{\mathrm{d} t} + 8 f(t) = 2 g(t)$

	b) $A \frac{\mathrm{d} V}{\mathrm{d} t} + B V = C t$

3. **Resuelve las siguientes ecuaciones diferenciales de primer orden:**

	a) $7 \frac{\mathrm{d} f(t)}{\mathrm{d} t} + 8 f(t) = 2$, $f(0) = 0$

	b) $7 \frac{\mathrm{d} f(t)}{\mathrm{d} t} + 8 f(t) = \sin(t)$, $f(0) = 0$

	c) $\frac{\mathrm{d}^2 y}{\mathrm{d} t^2} + 3 \frac{\mathrm{d} y}{\mathrm{d} t} +2 y = 1$, $y(0) = y'(0) = 1$


4. **Cómo influye en la dinámica de un proceso de primer orden la variación de:**

	a) La ganancia del proceso, $K_c$

	b) La constante de tiempo del proceso, $\tau$

5. **Determina la constante de tiempo y la ganancia del proceso del arduino (suponiendo que se trata de un sistema de primer orden).**






