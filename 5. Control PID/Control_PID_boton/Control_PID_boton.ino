const int lmPin = A0; // El pin de lectura del LM35 es el A0 (Analog 0)
const int TIP31 = 3; // El pin del TIP31
const int LED = 6;  // Pin del LED
const int BUTTON = 7;
int val = 0;
int old_val = 0;
float sp = 58; // Consigna
float stepSize = 0;
const long REFRESH_INTERVAL = 2000; // ms
long lastRefreshTime = 0;
unsigned long lastTime;
int calef = 0;
float Kc = 30;
float Ti = 20; // en s
float Td = 0;
int SampleTime = 1000; //1 s
float integralError = 0;
float lastError = 0;
float cP, cI, cD;

// kalman variables
float varVolt = 0.084317983645123;  // variance determined using excel and reading samples of raw sensor data
float varProcess = 1e-5;
float Pc = 0.0;
float G = 0.0;
float P = 1.0;
float Xp = 0.0;
float Zp = 0.0;
float Xe = 0.0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(19200); // Abre el monitor serie, velocidad 9600
  pinMode(LED, OUTPUT);
  // pinMode(BUTTON, INPUT);
  //pinMode(TIP31, OUTPUT);
}

float tempC() // Función de lectura de la temperatura
{
  float raw = analogRead(lmPin);
  float percent = raw / 1023.0;
  float voltage = percent * 5;

  // kalman process
  Pc = P + varProcess;
  G = Pc/(Pc + varVolt);    // kalman gain
  P = (1-G)*Pc;
  Xp = Xe;
  Zp = Xp;
  Xe = G*(voltage-Zp)+Xp;   // the kalman estimate of the sensor voltage
  
  return Xe*100.0-273.15;
}

void loop() {
  // put your main code here, to run repeatedly:

//  //val = digitalRead(BUTTON);
//  if (Serial.available()){
//    val = Serial.read();
//  }
//  //if ((val == HIGH) && (old_val == LOW)){
//  if (val == 1 and old_val == 0){
//    stepSize *= -1;
//    sp += stepSize;
//    integralError = 0;
//    delay(10);
//  } else {
//    delay(10);
//  }
//  old_val = val;
  // How long since we last calculated
  unsigned long now = millis();
  float timeChange = (now-lastTime);
  if (timeChange >= SampleTime)
  {
    float error = sp - tempC();
    integralError += error;
    float derror = (error - lastError);
    cP = Kc*error;
    if (Ti == 0){
      cI = 0;
    } else {
      cI = Kc/Ti*integralError;
    }
    cD = Kc*Td*derror;
    calef = cP + cI + cD;
    if (calef >255){
      calef = 255;
      integralError = 0;
    }
    if (calef < 0){
      calef = 0;
      integralError = 0;
    }
    lastError = error;
    lastTime = now;
    analogWrite(TIP31,calef);
    analogWrite(LED,calef);
  }
  
  if (millis() - lastRefreshTime >= REFRESH_INTERVAL){
    lastRefreshTime += REFRESH_INTERVAL;
    Serial.print(now);
    Serial.print(", ");
    Serial.print(calef);
    Serial.print(", ");
    Serial.print(tempC());
    Serial.print(", ");
    Serial.print(sp);
    Serial.print(", ");
    Serial.print(cP);
    Serial.print(", ");
    Serial.print(cI);
    Serial.print(", ");
    Serial.println(cD);
  }

  
}
